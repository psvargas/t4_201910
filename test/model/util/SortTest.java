
package model.util;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import model.util.Sort;
import model.vo.VOMovingViolation;

public class SortTest {

	// Muestra de datos a ordenar
	private Comparable<VOMovingViolation>[] datos;

	private Comparable <VOMovingViolation>[] ordenado;


	@Before
	public void setUpEscenario0() 
	{
		//Crea el arreglo a ordenar con infracciones agregadas de forma aleatoria.
		ArrayList<Integer> numeros = new ArrayList<>();
		for (int i=0;i<9;i++)
		{
			numeros.add(i);
		}
		Random random = new Random();
		while (numeros.size()>1)
		{
			for (int i=1;i<10;i++)
			{
				int randomIndex = random.nextInt(numeros.size());
				datos[randomIndex]= new VOMovingViolation(i, "a", 0, 0, 0, 0, 0, "b", "2018-01-0"+"i"+"T11:23:00.000Z", "c", "d");
				numeros.remove(randomIndex);
			}
		}
		//Crea el arreglo ordenado con el cual se verifica que los mÈtodos funcionan.
		for (int i=1;i<10;i++)
		{
			ordenado[i--]= new VOMovingViolation(i, "a", 0, 0, 0, 0, 0, "b", "2018-01-0"+"i"+"T11:23:00.000Z", "c", "d");
		}
	}

	public void setUpEscenario1() 
	{
		//Crea el arreglo a ordenar con infracciones agregadas de forma aleatoria.
		ArrayList<Integer> numeros = new ArrayList<>();
		for (int i=0;i<49;i++)
		{
			numeros.add(i);
		}
		Random random = new Random();
		while (numeros.size()>1)
		{
			for (int i=10;i<51;i++)
			{
				int randomIndex = random.nextInt(numeros.size());
				datos[randomIndex]= new VOMovingViolation(i, "a", 0, 0, 0, 0, 0, "b", "2018-01-"+"i"+"T11:23:00.000Z", "c", "d");
				numeros.remove(randomIndex);
			}
		}
		for (int i=10;i<51;i++)
		{
			ordenado[i--]= new VOMovingViolation(i, "a", 0, 0, 0, 0, 0, "b", "2018-01-"+"i"+"T11:23:00.000Z", "c", "d");
		}
	}
	@Test
	public void testMergeSort() 
	{
		setUpEscenario0();
		Sort.ordenarMergeSort(datos);
		assertArrayEquals(datos,ordenado);

		for(int i=0;i<datos.length;i++)
		{
			datos[i]=null;
		}
		setUpEscenario1();
		Sort.ordenarMergeSort(datos);
		assertArrayEquals(datos,ordenado);
	}
	public void testQuickSort() 
	{
		setUpEscenario0();
		Sort.ordenarQuickSort(datos);
		assertArrayEquals(datos,ordenado);

		for(int i=0;i<datos.length;i++)
		{
			datos[i]=null;
		}
		setUpEscenario1();
		Sort.ordenarQuickSort(datos);
		assertArrayEquals(datos,ordenado);
	}
	public void testShellSort() 
	{
		setUpEscenario0();
		Sort.ordenarShellSort(datos);
		assertArrayEquals(datos,ordenado);

		for(int i=0;i<datos.length;i++)
		{
			datos[i]=null;
		}
		setUpEscenario1();
		Sort.ordenarShellSort(datos);
		assertArrayEquals(datos,ordenado);
	}
}

