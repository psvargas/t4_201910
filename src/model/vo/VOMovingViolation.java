package model.vo;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Representation of a MovingViolation object
 */
public class VOMovingViolation implements Comparable<VOMovingViolation> 
{

	private  int objectId;
	private String location;
	private int addressId;
	private int streetId;
	private double fine;
	private double totalPaid;
	private double penalty1;
	private String accidentIndicator;
	private String ticketIssueDate;
	private String violationCode;
	private String description;


	public VOMovingViolation (int pObId, String pLoc, int pAddress, int pStreet, double pFine, double pTotalPaid, double pPenalty, String pAId, String pTdate, String pVCode, String pDesc )
	{
		objectId = pObId;
		location = pLoc;
		addressId = pAddress;
		streetId = pStreet;
		fine = pFine;
		totalPaid = pTotalPaid;
		penalty1 = pPenalty;
		accidentIndicator = pAId;
		ticketIssueDate = pTdate;
		violationCode = pVCode;
		description = pDesc;
	}
	/**
	 * @return id - Identificador único de la infracción
	 */
	public int objectId() 
	{
		return objectId;
	}	

	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() 
	{
		return location;
	}
	/**
	 * @return addressId - Id de la dirección.
	 */
	public int getAddressId()
	{
		return addressId;
	}
	/**
	 * @return streetId - Id del segmento de la calle.
	 */
	public int getStreetId()
	{
		return streetId;
	}
	/**
	 * @return fine - cantidad a pagar por la infracción.
	 */
	public double getFine()
	{
		return fine;
	}
	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
	 */
	public double getTotalPaid() 
	{
		return totalPaid;
	}
	/**
	 * @return penalty1 - Dinero extra que debe pagar el conductor.
	 */
	public double getPenalty1()
	{
		return penalty1;
	}
	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() 
	{
		return accidentIndicator;
	}
	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public String getTicketIssueDate() 
	{
		return ticketIssueDate;
	}
	/**
	 * @return violationCode - código de la infracción.
	 */
	public String getViolationCode()
	{
		return violationCode;
	}
	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDescription() 
	{
		return description;
	}

	public String toString()
	{
		// TODO Convertir objeto en String (representacion que se muestra en la consola)

		return objectId + "," + ticketIssueDate;
	}
	@Override
	/*
	 * Compara el ticketIssusDate de dos objetos, si son iguales los compara por el objectId.
	 * @param VOMovingViolation o - objeto con el que se quiere comparar el actual.
	 * @return 1 si el objeto actual es mayor que el que entra por parámetro, 
	 *        -1 si el actual es menor que el que entra por parámetro.
	 */
	public int compareTo(VOMovingViolation o)
	{

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"); 
		int retorno = 0;
		
		Date date = null;
		Date pDate = null;
		try 
		{
			date = sdf.parse(this.ticketIssueDate);
			pDate = sdf.parse(o.ticketIssueDate);
		} 
		catch (ParseException e)
		{
			e.printStackTrace();
		}
		
		if (date.compareTo(pDate) > 0 )
		{
			retorno = 1;
		}
		else if(date.compareTo(pDate) <0 )
		{
			retorno = -1;
		}
		else
		{
			if(this.objectId > o.objectId)
			{
				retorno = 1;
			}
			else 
			{
				retorno = -1;
			}
		}
		return retorno;
	}
}
