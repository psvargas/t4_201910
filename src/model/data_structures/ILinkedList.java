package model.data_structures;

public interface ILinkedList<T> extends Iterable<T>, Comparable<T>
{
	/**
	 * Retorna el tamaño de la lista.
	 * @return
	 */
	public int getSize();
	/**
	 * Indica si la lista esta vacia o no.
	 * @return
	 */
	public boolean isEmpty();
	/**
	 * Retorna el primer elemento del arreglo.
	 * @return
	 */
	public NodeLinkedList<T> darPrimero();
	/**
	 * Retorna el ultimo elemento del arreglo.
	 */
	public NodeLinkedList<T> darUltimo();
	/**
	 * Agrega un elemento entre 2 elementos.
	 */
	public void agregarEntre(T pElm, NodeLinkedList<T> pP, NodeLinkedList<T> pS);
	/**
	 * Agrega un elemento
	 */
	public void agregar(T dato);

	/**
	 * Agregar un elemento al final
	 * @param dato
	 */
	public void agregarAlFinal(T dato);
	/**
	 * Agrega un elemento en la posición dada.
	 * @param dato
	 */
	public T eliminar(NodeLinkedList<T> nodo);
	/**
	 * Retorna el elemento que esta en la posicion dada por parámetro.
	 * @param n Posicion 
	 * @return Elemento
	 */
	public T darElemento(int n);





}
