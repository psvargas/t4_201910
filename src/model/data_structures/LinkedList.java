package model.data_structures;
import java.io.Serializable;
import java.util.Iterator;

public class LinkedList<T> implements ILinkedList<T>, Serializable
{

	private NodeLinkedList<T> cabeza;
	private NodeLinkedList<T> cola;
	private Integer tamano = 0;

	public LinkedList()
	{
		cabeza = new NodeLinkedList<T>(null, null, null);
		cola = new NodeLinkedList<T>(null, cabeza, null);
		cabeza.cambiarSiguiente(cola);

	}
	public int getSize() 
	{
		return tamano;
	}
	public boolean isEmpty()
	{
		return tamano == 0;
	}

	public NodeLinkedList<T> darPrimero()
	{
		if(isEmpty() == true)
			return null;
		else
			return cabeza.darSiguiente();
	}

	public NodeLinkedList<T> darUltimo()
	{
		if(isEmpty() == true)
			return null;
		else
			return cola.darAnterior();
	}

	public void agregarEntre(T elemento, NodeLinkedList<T> primero, NodeLinkedList<T> segundo)
	{
		NodeLinkedList<T> nuevo = new NodeLinkedList<T>(elemento, primero, segundo);
		primero.cambiarSiguiente(nuevo);
		segundo.cambiarAnterior(nuevo);
		tamano++;
	}
	public void agregar(T pElemento) 
	{
		agregarEntre(pElemento, cabeza, cabeza.darSiguiente());
	}

	public void agregarAlFinal(T pElemento) 
	{
		agregarEntre(pElemento, cola.darAnterior(), cola);
	}
	
	public T eliminar(NodeLinkedList<T> nodo)
	{
		NodeLinkedList<T> anterior = nodo.darAnterior();
		NodeLinkedList<T> siguiente = nodo.darSiguiente();

		anterior.cambiarSiguiente(siguiente);
		siguiente.cambiarAnterior(anterior);
		nodo.cambiarSiguiente(null);
		nodo.cambiarAnterior(null);
		tamano--;
		return nodo.darElemento();
	}
	
	public Iterator<T> iterator() 
	{
		return new IteradorLinkedList<>(cabeza.darSiguiente());
	}
	
	public T darElemento(int n)
	{
		
		NodeLinkedList<T> actual = (NodeLinkedList<T>) cabeza;
		T elemento = actual.darElemento();
		for(int i=0; i<n && actual != null; i++)
		{
			actual = actual.darSiguiente();
			elemento = actual.darElemento();
		}

		return elemento;
	}
	
	@Override
	public int compareTo(T o) 
	{
		//TODO
		return 0;
	}
}
