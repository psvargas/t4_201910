package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class IteradorLinkedList<T> implements Iterator<T>
{
	private NodeLinkedList<T> actual;

	public IteradorLinkedList(NodeLinkedList<T> pPrimero)
	{
		NodeLinkedList<T> actual = pPrimero;
	}

	public boolean hasNext() 
	{
		if (actual.darSiguiente() == null)
		{
			return false;
		}
		return true;
	}

	public T next() 
	{
		if (hasNext()==false)
		{
			throw new NoSuchElementException("No hay un siguiente.");
		}

		T e = actual.darElemento();
		actual = actual.darSiguiente(); 
		return e;
	}
}